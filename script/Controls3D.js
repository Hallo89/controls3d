'use strict';
export class Controls3D {
    static MODIFIER_KEYS = ['shiftKey', 'ctrlKey', 'altKey', 'metaKey'];
    // Persistent transform properties between events
    #pointerInit = null;
    #touchInit = null;
    hasGamepad = false;
    gamepads = {};
    /** If true, a touch action is currently underway. */
    touchIsActive = false;
    config = {
        target: null,
        state: null,
        callback: null,
        mod: {
            scale: .224,
            tran: 1,
            rot: 1
        },
        gamepadMod: {
            scale: .015,
            tran: .25,
            rot: .75
        },
        // TODO The gamepad buttons are not configurable yet
        buttons: {
            tran: 0,
            rot: 2
        },
        joystickThreshold: .14,
        disableEvents: false,
        invertTranslateY: false,
        keyModifier: {
            scale: {
                x: [[], ['ctrlKey']],
                y: [[], ['shiftKey']],
                z: [[], ['ctrlKey', 'shiftKey']]
            },
            tran: {
                x: [[]],
                y: [[]],
            },
            rot: {
                x: [[]],
                y: [[]],
            }
        }
    };
    target = null;
    state = null;
    callback = null;
    /**
     * @param eventTarget The DOM Element to receive the pointer, wheel and touch events.
     *                    If left unspecified, use {@link changeTarget}.
     * @param config A subset of the possible configuration options.
     */
    constructor(eventTarget, config) {
        if (config?.state) {
            this.changeState(config.state);
            delete config.state;
        }
        if (config?.target) {
            this.changeTarget(config.target);
            delete config.target;
        }
        if (config?.callback) {
            this.changeCallback(config.callback);
            delete config.callback;
        }
        if (config) {
            this.assignNewConfig(config);
        }
        this.preventContext = this.preventContext.bind(this);
        this.touchDown = this.touchDown.bind(this);
        this.touchMove = this.touchMove.bind(this);
        this.touchUp = this.touchUp.bind(this);
        this.pointerDown = this.pointerDown.bind(this);
        this.pointerMove = this.pointerMove.bind(this);
        this.wheelAction = this.wheelAction.bind(this);
        this.gamepadLoop = this.gamepadLoop.bind(this);
        if (this.config.disableEvents !== true) {
            if (!this.config.disableEvents || this.config.disableEvents.pointer) {
                window.addEventListener('pointerup', this.pointerUp.bind(this), true);
            }
            if (!this.config.disableEvents || this.config.disableEvents.gamepad) {
                window.addEventListener('gamepadconnected', this.gamepadConnected.bind(this));
                window.addEventListener('gamepaddisconnected', this.gamepadDisconnected.bind(this));
            }
        }
    }
    // ---- Context switching functions ----
    /**
     * Assign a new event target.
     * This removes currently attached events and attaches them to the new target.
     */
    changeTarget(newEventTarget) {
        if (this.target) {
            this.removeTargetEvents(this.target);
        }
        this.addTargetEvents(newEventTarget);
        // @ts-ignore
        this.target = newEventTarget;
    }
    changeState(newState) {
        // @ts-ignore
        this.state = newState;
    }
    changeCallback(newCallback) {
        // @ts-ignore
        this.callback = newCallback;
    }
    // ---- Helper functions ----
    assignNewConfig(newConfig) {
        // Dynamically assigns new fields up to 1 nested object deep
        for (const [configName, configVal] of Object.entries(newConfig)) {
            if (!(configName in this.config)) {
                throw new Error(`Controls3D: Config option '${configName}' does not exist!`);
            }
            if (typeof configVal === 'object') {
                this.config[configName] = Object.assign(this.config[configName], configVal);
            }
            else {
                this.config[configName] = configVal;
            }
        }
    }
    removeTargetEvents(eventTarget) {
        eventTarget.removeEventListener('contextmenu', this.preventContext);
        eventTarget.removeEventListener('pointerdown', this.pointerDown, true);
        eventTarget.removeEventListener('wheel', this.wheelAction);
        eventTarget.removeEventListener('touchstart', this.touchDown, true);
        eventTarget.removeEventListener('touchmove', this.touchMove, true);
        eventTarget.removeEventListener('touchend', this.touchUp, true);
        eventTarget.removeEventListener('touchcancel', this.touchUp, true);
    }
    addTargetEvents(eventTarget) {
        const conf = this.config.disableEvents;
        if (conf !== true) {
            if (!conf || conf.contextmenu) {
                eventTarget.addEventListener('contextmenu', this.preventContext);
            }
            if (!conf || conf.pointer) {
                eventTarget.addEventListener('pointerdown', this.pointerDown, true);
            }
            if (!conf || conf.wheel) {
                eventTarget.addEventListener('wheel', this.wheelAction);
            }
            if (!conf || conf.touch) {
                eventTarget.addEventListener('touchstart', this.touchDown, true);
                eventTarget.addEventListener('touchmove', this.touchMove, true);
                eventTarget.addEventListener('touchend', this.touchUp, true);
                eventTarget.addEventListener('touchcancel', this.touchUp, true);
            }
        }
    }
    // ---- Touch events ----
    touchDown(e) {
        if (e.targetTouches.length === 2) {
            e.preventDefault();
            this.initializeTouchData(e.targetTouches);
            this.touchIsActive = true;
        }
    }
    touchMove(e) {
        if (e.targetTouches.length >= 2) {
            e.preventDefault();
            const usedTouches = this.getTouchesFromIDs(e.targetTouches, this.#touchInit.ids);
            this.touchTransformTranslate(usedTouches);
            this.touchTransformScale(usedTouches);
            this.callback({ touches: usedTouches });
            // Update touch data in case the state has been changed from the outside
            // @ts-ignore
            this.updateTouchData(usedTouches);
        }
    }
    touchUp(e) {
        if (e.targetTouches.length === 2) {
            // Update touch data to the current fingers, in case a finger was
            // lifted which was responsible for the previous initial data
            this.initializeTouchData(e.targetTouches);
        }
        else if (e.targetTouches.length < 2) {
            this.#touchInit = null;
            this.touchIsActive = false;
        }
    }
    // ---- Touch transform functions ----
    touchTransformTranslate(usedTouches) {
        const usedMidpoint = Controls3D.getTouchesMidpoint(...usedTouches);
        const initialMidpoint = Controls3D.getTouchesMidpoint(...this.#touchInit.data);
        const averageDistance = {
            x: usedMidpoint[0] - initialMidpoint[0],
            y: usedMidpoint[1] - initialMidpoint[1]
        };
        this.state.assignNewTransform({
            tran: {
                x: this.#touchInit.tran.x + averageDistance.x,
                y: this.#touchInit.tran.y + averageDistance.y,
            }
        });
    }
    touchTransformScale(usedTouches) {
        const distance = Controls3D.getTouchesDistance(...usedTouches);
        this.state.assignNewTransform({
            scale: {
                x: distance / this.#touchInit.distance.x,
                y: distance / this.#touchInit.distance.y,
                z: distance / this.#touchInit.distance.z
            }
        });
    }
    // ---- Touch helper functions ----
    // NOTE: It is assumed that all given touchIDs are valid
    getTouchesFromIDs(targetTouches, touchIDs) {
        const usedTouches = new Array(touchIDs.length);
        for (let i = 0; i < usedTouches.length; i++) {
            for (const targetTouch of targetTouches) {
                if (targetTouch.identifier === touchIDs[i]) {
                    usedTouches[i] = targetTouch;
                }
            }
        }
        return usedTouches;
    }
    initializeTouchData(touchList) {
        // @ts-ignore Only initialization
        this.#touchInit = {
            ids: [
                touchList[0].identifier,
                touchList[1].identifier
            ],
        };
        this.updateTouchData(touchList);
    }
    updateTouchData(touchList) {
        const distance = Controls3D.getTouchesDistance(touchList[0], touchList[1]);
        this.#touchInit.tran = Object.assign({}, this.state.tran);
        this.#touchInit.distance = {
            x: distance / this.state.scale.x,
            y: distance / this.state.scale.y,
            z: distance / this.state.scale.z
        };
        // NOTE Touch objects are supposed to be immutable, but apparently apple reuses
        // them or have reused them at some point. By copying them, I'm going the safe route.
        this.#touchInit.data = [
            { clientX: touchList[0].clientX, clientY: touchList[0].clientY },
            { clientX: touchList[1].clientX, clientY: touchList[1].clientY }
        ];
    }
    // ---- Misc events ----
    preventContext(e) {
        e.preventDefault();
    }
    async wheelAction(e) {
        const usedAxes = Controls3D.getEligibleAxesFromKeyMap(this.config.keyModifier.scale, e);
        if (usedAxes.length > 0 && e.deltaY !== 0) {
            const direction = -1 * (e.deltaY / Math.abs(e.deltaY));
            const transform = {};
            for (const axis of usedAxes) {
                transform[axis] = direction * this.config.mod.scale;
            }
            await this.state.animateTo(45, { scale: transform }, {
                useStacking: true,
                callback: this.callback
            });
        }
    }
    // ---- Pointer events ----
    pointerMove(e) {
        switch (this.#pointerInit.button) {
            case this.config.buttons.tran: {
                const usedAxes = Controls3D.getEligibleAxesFromKeyMap(this.config.keyModifier.tran, e);
                this.pointerTransformTranslate(e, usedAxes);
                break;
            }
            case this.config.buttons.rot: {
                const usedAxes = Controls3D.getEligibleAxesFromKeyMap(this.config.keyModifier.rot, e);
                this.pointerTransformRotate(e, usedAxes);
                break;
            }
        }
    }
    pointerDown(e) {
        if (e.button === 1)
            e.preventDefault();
        this.#pointerInit = {
            x: e.screenX,
            y: e.screenY,
            button: e.button,
            tran: Object.assign({}, this.state.tran),
            rot: Object.assign({}, this.state.rot),
        };
        window.addEventListener('pointermove', this.pointerMove);
    }
    pointerUp() {
        window.removeEventListener('pointermove', this.pointerMove);
        this.#pointerInit = null;
    }
    // ---- Pointer transform functions ----
    pointerTransformTranslate(e, usedAxes) {
        const distance = {};
        if (usedAxes.includes('x')) {
            distance.x = this.#pointerInit.tran.x + (e.screenX - this.#pointerInit.x) * this.config.mod.tran;
        }
        if (usedAxes.includes('y')) {
            const modY = this.config.mod.tran * (this.config.invertTranslateY ? -1 : 1);
            distance.y = this.#pointerInit.tran.y + (e.screenY - this.#pointerInit.y) * modY;
        }
        if (distance.x || distance.y) {
            this.assignNewTransformAndDraw({
                tran: distance
            });
        }
    }
    pointerTransformRotate(e, usedAxes) {
        // TODO This could possibly be configuration option:
        // x and y are swapped because of the OpenGL 3D coordinate system axes
        const distance = {};
        if (usedAxes.includes('x')) {
            distance.x = this.#pointerInit.rot.x + (e.screenY - this.#pointerInit.y) * this.config.mod.rot;
        }
        if (usedAxes.includes('y')) {
            distance.y = this.#pointerInit.rot.y + (e.screenX - this.#pointerInit.x) * this.config.mod.rot;
        }
        if (distance.x || distance.y) {
            this.assignNewTransformAndDraw({
                rot: distance
            });
        }
    }
    // ---- Gamepad handling ----
    gamepadConnected(e) {
        this.gamepads[e.gamepad.index] = e.gamepad;
        this.hasGamepad = true;
        this.gamepadLoop();
    }
    gamepadDisconnected(e) {
        delete this.gamepads[e.gamepad.index];
        if (Object.keys(this.gamepads).length == 0) {
            this.hasGamepad = false;
        }
    }
    gamepadLoop() {
        for (const gamepadIndex in this.gamepads) {
            const gamepad = this.gamepads[gamepadIndex];
            // Scale, LB/RB
            if (gamepad.buttons[4].pressed || gamepad.buttons[5].pressed) {
                const direction = gamepad.buttons[5].value || -gamepad.buttons[4].value;
                if (direction) {
                    this.state.animateTo(35, {
                        scale: {
                            x: direction * this.config.gamepadMod.scale,
                            y: direction * this.config.gamepadMod.scale,
                            z: direction * this.config.gamepadMod.scale
                        }
                    }, {
                        callback: this.callback,
                        useStacking: true,
                    });
                }
            }
            // Translate, left joystick
            this.handleJoystick([gamepad.axes[0], -gamepad.axes[1]], gamepad.buttons[10], 'tran');
            // Rotate, right joystick
            this.handleJoystick([gamepad.axes[3], gamepad.axes[2]], gamepad.buttons[11], 'rot');
        }
        if (this.hasGamepad) {
            requestAnimationFrame(this.gamepadLoop);
        }
    }
    handleJoystick(axes, resetButton, type) {
        let hasNewState = false;
        const newState = {
            [type]: {}
        };
        if (axes[0] > this.config.joystickThreshold || axes[0] < -this.config.joystickThreshold) {
            hasNewState = true;
            newState[type].x = (axes[0] * this.config.gamepadMod[type]) + this.state[type].x;
        }
        if (axes[1] > this.config.joystickThreshold || axes[1] < -this.config.joystickThreshold) {
            hasNewState = true;
            newState[type].y = (axes[1] * this.config.gamepadMod[type]) + this.state[type].y;
        }
        if (hasNewState) {
            this.assignNewTransformAndDraw(newState);
        }
        // Reset distance when right joystick is pressed
        if (resetButton.pressed) {
            newState[type].x = 0;
            newState[type].y = 0;
            this.assignNewTransformAndDraw(newState);
        }
    }
    // ---- Helper function ----
    assignNewTransformAndDraw(transform) {
        this.state.assignNewTransform(transform);
        this.callback();
    }
    // ---- Static helpers ----
    static getEligibleAxesFromKeyMap(keyMap, e) {
        const usedAxes = [];
        for (const [axis, axisKeyList] of Object.entries(keyMap)) {
            for (const axisKeys of axisKeyList) {
                // Check if every present and non present axis modifier matches e exactly
                if (Controls3D.MODIFIER_KEYS.every(modKey => e[modKey] === axisKeys.includes(modKey))) {
                    usedAxes.push(axis);
                    if (axisKeys.includes('ctrlKey')) {
                        e.preventDefault();
                    }
                    break;
                }
            }
        }
        return usedAxes;
    }
    /** Compute the distance between two supplied Touch objects. */
    static getTouchesDistance(touch0, touch1) {
        return Math.sqrt(Math.pow(touch1.screenX - touch0.screenX, 2)
            + Math.pow(touch1.screenY - touch0.screenY, 2));
    }
    /** Compute the midpoint vector of two supplied Touch objects. */
    static getTouchesMidpoint(touch0, touch1) {
        return [
            (touch0.clientX + touch1.clientX) / 2,
            (touch0.clientY + touch1.clientY) / 2,
        ];
    }
}
