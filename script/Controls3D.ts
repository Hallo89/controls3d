'use strict';
import {
  State3D,
  Animation,
  Transform,
  TransformType,
  TransformSingle
}  from './State3D.js';

export type TransformModfier = Record<TransformType, number>;
export type KeyboardModifier<Axes extends string> = Record<Axes, Array<Array<string>>>;

export namespace Touch {
  export type TouchData = { clientX: number, clientY: number };

  export type TouchIDList = [number, number];
  export type UsedTouchList = [Touch, Touch];
  export type TouchDataList = [TouchData, TouchData];
}

interface Distance2D {
  x: number;
  y: number;
}

export interface ClickState {
  x: number;
  y: number;
  button: number;
  tran: TransformSingle;
  rot: TransformSingle;
}
export interface TouchState {
  ids: Touch.TouchIDList;
  data: Touch.TouchDataList;
  tran: TransformSingle;
  distance: TransformSingle;
}

export interface DrawData extends Partial<Animation.AnimationData> {
  touches?: Touch.UsedTouchList;
}

export interface Controls3DConfig {
  /**
   * DOM element that will register all pointer, mousewheel
   * and touch events used for transformation.
   *
   * If left unspecified, set it later using {@link Controls3D.changeTarget}.
   */
  target: EventTarget | null;
  /**
   * The initial state all transformations will operate on.
   *
   * If left unspecified, set it later using {@link Controls3D.changeState}.
   */
  state: State3D | null;
  /**
   * The initial callback that will be called during transformations.
   *
   * If left unspecified, set it later using {@link Controls3D.changeCallback}.
   */
  callback: ((data?: DrawData) => void) | null;
  /**
   * Modifier that will be multiplied with the value
   * of *pointer* transformation operations.
   *
   * For example, a `tran` mod of .25 will reduce the pointer translation
   * values per call to 25%, slowing down the translation.
   */
  mod: TransformModfier;
  /** Like {@link mod} but only for gamepad transformations. */
  gamepadMod: TransformModfier;
  /**
   * Mouse buttons that invoke the specified states during a pointer event.
   * @see {@link MouseEvent.button}
   */
  buttons: {
    tran: number,
    rot: number,
  };
  /** Disable all or only specific event types. */
  disableEvents: boolean | {
    /** Disable the prevention of the context menu on right click. */
    contextmenu: boolean,
    /** Disable gamepad integration. */
    gamepad: boolean,
    /** Disable all pointer events */
    pointer: boolean,
    /** Disable the scroll wheel event which would invoke a scale. */
    wheel: boolean,
    /** Disable all touch events. */
    touch: boolean,
  };
  /**
   * Configure gamepad joystick dead zone threshold
   * in order to prevent passive drift.
   *
   * Accepts a range from 0 to 1, corresponding to a threshold of
   * 0% and 100% respectively.
   */
  joystickThreshold: number;
  /**
   * Whether to flip the Y axis within translation.
   *
   * You'll want to enable this option when using Controls3D
   * with OpenGL since it uses some counterintuitive axis math.
   */
  invertTranslateY: boolean;
  /**
   * Configure *mandatory* keyboard modifiers for a specified state and axis
   * to be able to get modified during a pointer event.
   *
   * - The array can contain multiple key mod arrays, any of which must be valid.
   * - A key mod array must match the event exactly; Non-present key modifiers
   *   inside it must NOT be pressed in order for the axis to be registered.
   *   See the example for more info.
   *
   * @example The default `scale` key modifiers.
   * With this configuration, all `scale` axes are modified simultaneously
   * upon scroll (as is the expected behavior), but as soon as the ctrl key
   * is pressed, ONLY the X axis is changed.
   * ```js
   * keyModifier: {
   *   scale: {
   *     x: [ [], [ 'ctrlKey' ] ],
   *     y: [ [], [ 'shiftKey' ] ],
   *     z: [ [], [ 'ctrlKey', 'shiftKey' ] ]
   *   }
   * }
   * ```
   *
   * @see {@link Controls3D.MODIFIER_KEYS} for the available values.
   */
  keyModifier: {
    scale: KeyboardModifier<'x' | 'y' | 'z'>,
    tran: KeyboardModifier<'x' | 'y'>,
    rot: KeyboardModifier<'x' | 'y'>,
  };
}

export class Controls3D {
  static MODIFIER_KEYS = [ 'shiftKey', 'ctrlKey', 'altKey', 'metaKey' ] as const;

  // Persistent transform properties between events
  #pointerInit: ClickState | null = null;
  #touchInit: TouchState | null = null;

  hasGamepad = false;
  gamepads: Record<number, Gamepad> = {};

  /** If true, a touch action is currently underway. */
  touchIsActive = false;

  config: Controls3DConfig = {
    target: null,
    state: null,
    callback: null,
    mod: {
      scale: .224,
      tran: 1,
      rot: 1
    },
    gamepadMod: {
      scale: .015,
      tran: .25,
      rot: .75
    },
    // TODO The gamepad buttons are not configurable yet
    buttons: {
      tran: 0,
      rot: 2
    },
    joystickThreshold: .14,
    disableEvents: false,
    invertTranslateY: false,
    keyModifier: {
      scale: {
        x: [ [], [ 'ctrlKey' ] ],
        y: [ [], [ 'shiftKey' ] ],
        z: [ [], [ 'ctrlKey', 'shiftKey' ] ]
      },
      tran: {
        x: [ [] ],
        y: [ [] ],
      },
      rot: {
        x: [ [] ],
        y: [ [] ],
      }
    }
  };

  readonly target: Controls3DConfig['target'] = null;
  readonly state: Controls3DConfig['state'] = null;
  readonly callback: Controls3DConfig['callback'] = null;

  /**
   * @param eventTarget The DOM Element to receive the pointer, wheel and touch events.
   *                    If left unspecified, use {@link changeTarget}.
   * @param config A subset of the possible configuration options.
   */
  constructor(eventTarget?: EventTarget, config?: Partial<Controls3DConfig>) {
    if (config?.state) {
      this.changeState(config.state);
      delete config.state;
    }
    if (config?.target) {
      this.changeTarget(config.target);
      delete config.target;
    }
    if (config?.callback) {
      this.changeCallback(config.callback);
      delete config.callback;
    }
    if (config) {
      this.assignNewConfig(config);
    }

    this.preventContext = this.preventContext.bind(this);
    this.touchDown = this.touchDown.bind(this);
    this.touchMove = this.touchMove.bind(this);
    this.touchUp = this.touchUp.bind(this);
    this.pointerDown = this.pointerDown.bind(this);
    this.pointerMove = this.pointerMove.bind(this);
    this.wheelAction = this.wheelAction.bind(this);
    this.gamepadLoop = this.gamepadLoop.bind(this);

    if (this.config.disableEvents !== true) {
      if (!this.config.disableEvents || this.config.disableEvents.pointer) {
        window.addEventListener('pointerup', this.pointerUp.bind(this), true);
      }
      if (!this.config.disableEvents || this.config.disableEvents.gamepad) {
        window.addEventListener('gamepadconnected', this.gamepadConnected.bind(this));
        window.addEventListener('gamepaddisconnected', this.gamepadDisconnected.bind(this));
      }
    }
  }

  // ---- Context switching functions ----
  /**
   * Assign a new event target.
   * This removes currently attached events and attaches them to the new target.
   */
  changeTarget(newEventTarget: EventTarget) {
    if (this.target) {
      this.removeTargetEvents(this.target);
    }
    this.addTargetEvents(newEventTarget);
    // @ts-ignore
    this.target = newEventTarget;
  }
  changeState(newState: State3D) {
    // @ts-ignore
    this.state = newState;
  }
  changeCallback(newCallback: Function) {
    // @ts-ignore
    this.callback = newCallback;
  }

  // ---- Helper functions ----
  assignNewConfig(newConfig: Partial<Controls3DConfig>) {
    // Dynamically assigns new fields up to 1 nested object deep
    for (const [ configName, configVal ] of Object.entries(newConfig)) {
      if (!(configName in this.config)) {
        throw new Error(`Controls3D: Config option '${configName}' does not exist!`);
      }

      if (typeof configVal === 'object') {
        this.config[configName] = Object.assign(this.config[configName], configVal);
      } else {
        this.config[configName] = configVal;
      }
    }
  }

  removeTargetEvents(eventTarget: EventTarget) {
    eventTarget.removeEventListener('contextmenu', this.preventContext);
    eventTarget.removeEventListener('pointerdown', this.pointerDown, true);
    eventTarget.removeEventListener('wheel', this.wheelAction);

    eventTarget.removeEventListener('touchstart', this.touchDown, true);
    eventTarget.removeEventListener('touchmove', this.touchMove, true);
    eventTarget.removeEventListener('touchend', this.touchUp, true);
    eventTarget.removeEventListener('touchcancel', this.touchUp, true);
  }
  addTargetEvents(eventTarget: EventTarget) {
    const conf = this.config.disableEvents;
    if (conf !== true) {
      if (!conf || conf.contextmenu) {
        eventTarget.addEventListener('contextmenu', this.preventContext);
      }
      if (!conf || conf.pointer) {
        eventTarget.addEventListener('pointerdown', this.pointerDown, true);
      }
      if (!conf || conf.wheel) {
        eventTarget.addEventListener('wheel', this.wheelAction);
      }
      if (!conf || conf.touch) {
        eventTarget.addEventListener('touchstart', this.touchDown, true);
        eventTarget.addEventListener('touchmove', this.touchMove, true);
        eventTarget.addEventListener('touchend', this.touchUp, true);
        eventTarget.addEventListener('touchcancel', this.touchUp, true);
      }
    }
  }


  // ---- Touch events ----
  touchDown(e: TouchEvent) {
    if (e.targetTouches.length === 2) {
      e.preventDefault();
      this.initializeTouchData(e.targetTouches);
      this.touchIsActive = true;
    }
  }
  touchMove(e: TouchEvent) {
    if (e.targetTouches.length >= 2) {
      e.preventDefault();

      const usedTouches = this.getTouchesFromIDs(e.targetTouches, this.#touchInit.ids);
      this.touchTransformTranslate(usedTouches);
      this.touchTransformScale(usedTouches);

      this.callback({ touches: usedTouches } as DrawData);

      // Update touch data in case the state has been changed from the outside
      // @ts-ignore
      this.updateTouchData(usedTouches);
    }
  }
  touchUp(e: TouchEvent) {
    if (e.targetTouches.length === 2) {
      // Update touch data to the current fingers, in case a finger was
      // lifted which was responsible for the previous initial data
      this.initializeTouchData(e.targetTouches);
    } else if (e.targetTouches.length < 2) {
      this.#touchInit = null;
      this.touchIsActive = false;
    }
  }

  // ---- Touch transform functions ----
  touchTransformTranslate(usedTouches: Touch.UsedTouchList) {
    const usedMidpoint = Controls3D.getTouchesMidpoint(...usedTouches);
    const initialMidpoint = Controls3D.getTouchesMidpoint(...this.#touchInit.data);

    const averageDistance = {
      x: usedMidpoint[0] - initialMidpoint[0],
      y: usedMidpoint[1] - initialMidpoint[1]
    };

    this.state.assignNewTransform({
      tran: {
        x: this.#touchInit.tran.x + averageDistance.x,
        y: this.#touchInit.tran.y + averageDistance.y,
      }
    });
  }
  touchTransformScale(usedTouches: Touch.UsedTouchList) {
    const distance = Controls3D.getTouchesDistance(...usedTouches);

    this.state.assignNewTransform({
      scale: {
        x: distance / this.#touchInit.distance.x,
        y: distance / this.#touchInit.distance.y,
        z: distance / this.#touchInit.distance.z
      }
    });
  }

  // ---- Touch helper functions ----
  // NOTE: It is assumed that all given touchIDs are valid
  getTouchesFromIDs(targetTouches: TouchList, touchIDs: Touch.TouchIDList): Touch.UsedTouchList {
    const usedTouches = new Array(touchIDs.length) as Touch.UsedTouchList;
    for (let i = 0; i < usedTouches.length; i++) {
      for (const targetTouch of targetTouches) {
        if (targetTouch.identifier === touchIDs[i]) {
          usedTouches[i] = targetTouch;
        }
      }
    }
    return usedTouches;
  }

  initializeTouchData(touchList: TouchList) {
    // @ts-ignore Only initialization
    this.#touchInit = {
      ids: [
        touchList[0].identifier,
        touchList[1].identifier
      ],
    }
    this.updateTouchData(touchList);
  }
  updateTouchData(touchList: TouchList) {
    const distance = Controls3D.getTouchesDistance(touchList[0], touchList[1]);

    this.#touchInit.tran = Object.assign({}, this.state.tran);
    this.#touchInit.distance = {
      x: distance / this.state.scale.x,
      y: distance / this.state.scale.y,
      z: distance / this.state.scale.z
    };
    // NOTE Touch objects are supposed to be immutable, but apparently apple reuses
    // them or have reused them at some point. By copying them, I'm going the safe route.
    this.#touchInit.data = [
      { clientX: touchList[0].clientX, clientY: touchList[0].clientY },
      { clientX: touchList[1].clientX, clientY: touchList[1].clientY }
    ];
  }

  // ---- Misc events ----
  preventContext(e: PointerEvent) {
    e.preventDefault();
  }

  async wheelAction(e: WheelEvent) {
    const usedAxes = Controls3D.getEligibleAxesFromKeyMap(this.config.keyModifier.scale, e);
    if (usedAxes.length > 0 && e.deltaY !== 0) {
      const direction = -1 * (e.deltaY / Math.abs(e.deltaY)) as 1 | -1;
      const transform = {} as Partial<TransformSingle>;

      for (const axis of usedAxes) {
        transform[axis] = direction * this.config.mod.scale;
      }
      await this.state.animateTo(45, { scale: transform }, {
        useStacking: true,
        callback: this.callback
      });
    }
  }

  // ---- Pointer events ----
  pointerMove(e: PointerEvent) {
    switch (this.#pointerInit.button) {
      case this.config.buttons.tran: {
        const usedAxes = Controls3D.getEligibleAxesFromKeyMap(this.config.keyModifier.tran, e);
        this.pointerTransformTranslate(e, usedAxes);
        break;
      }
      case this.config.buttons.rot: {
        const usedAxes = Controls3D.getEligibleAxesFromKeyMap(this.config.keyModifier.rot, e);
        this.pointerTransformRotate(e, usedAxes);
        break;
      }
    }
  }
  pointerDown(e: PointerEvent) {
    if (e.button === 1) e.preventDefault();
    this.#pointerInit = {
      x: e.screenX,
      y: e.screenY,
      button: e.button,
      tran: Object.assign({}, this.state.tran),
      rot: Object.assign({}, this.state.rot),
    };
    window.addEventListener('pointermove', this.pointerMove);
  }
  pointerUp() {
    window.removeEventListener('pointermove', this.pointerMove);
    this.#pointerInit = null;
  }

  // ---- Pointer transform functions ----
  pointerTransformTranslate(e: PointerEvent, usedAxes: Array<'x' | 'y'>) {
    const distance: Partial<Distance2D> = {};
    if (usedAxes.includes('x')) {
      distance.x = this.#pointerInit.tran.x + (e.screenX - this.#pointerInit.x) * this.config.mod.tran;
    }
    if (usedAxes.includes('y')) {
      const modY = this.config.mod.tran * (this.config.invertTranslateY ? -1 : 1);
      distance.y = this.#pointerInit.tran.y + (e.screenY - this.#pointerInit.y) * modY
    }
    if (distance.x || distance.y) {
      this.assignNewTransformAndDraw({
        tran: distance
      });
    }
  }
  pointerTransformRotate(e: PointerEvent, usedAxes: Array<'x' | 'y'>) {
    // TODO This could possibly be configuration option:
    // x and y are swapped because of the OpenGL 3D coordinate system axes
    const distance: Partial<Distance2D> = {};
    if (usedAxes.includes('x')) {
      distance.x = this.#pointerInit.rot.x + (e.screenY - this.#pointerInit.y) * this.config.mod.rot;
    }
    if (usedAxes.includes('y')) {
      distance.y = this.#pointerInit.rot.y + (e.screenX - this.#pointerInit.x) * this.config.mod.rot;
    }
    if (distance.x || distance.y) {
      this.assignNewTransformAndDraw({
        rot: distance
      });
    }
  }


  // ---- Gamepad handling ----
  gamepadConnected(e: GamepadEvent) {
    this.gamepads[e.gamepad.index] = e.gamepad;
    this.hasGamepad = true;
    this.gamepadLoop();
  }
  gamepadDisconnected(e: GamepadEvent) {
    delete this.gamepads[e.gamepad.index];
    if (Object.keys(this.gamepads).length == 0) {
      this.hasGamepad = false;
    }
  }

  gamepadLoop() {
    for (const gamepadIndex in this.gamepads) {
      const gamepad = this.gamepads[gamepadIndex];

      // Scale, LB/RB
      if (gamepad.buttons[4].pressed || gamepad.buttons[5].pressed) {
        const direction = gamepad.buttons[5].value || -gamepad.buttons[4].value;
        if (direction) {
          this.state.animateTo(35, {
            scale: {
              x: direction * this.config.gamepadMod.scale,
              y: direction * this.config.gamepadMod.scale,
              z: direction * this.config.gamepadMod.scale
            }
          }, {
            callback: this.callback,
            useStacking: true,
          });
        }
      }

      // Translate, left joystick
      this.handleJoystick([gamepad.axes[0], -gamepad.axes[1]], gamepad.buttons[10], 'tran');

      // Rotate, right joystick
      this.handleJoystick([gamepad.axes[3], gamepad.axes[2]], gamepad.buttons[11], 'rot');
    }

    if (this.hasGamepad) {
      requestAnimationFrame(this.gamepadLoop);
    }
  }

  handleJoystick(axes: [number, number], resetButton: GamepadButton, type: TransformType) {
    let hasNewState = false;
    const newState: Transform = {
      [type]: {}
    };

    if (axes[0] > this.config.joystickThreshold || axes[0] < -this.config.joystickThreshold) {
      hasNewState = true;
      newState[type].x = (axes[0] * this.config.gamepadMod[type]) + this.state[type].x;
    }
    if (axes[1] > this.config.joystickThreshold || axes[1] < -this.config.joystickThreshold) {
      hasNewState = true;
      newState[type].y = (axes[1] * this.config.gamepadMod[type]) + this.state[type].y;
    }

    if (hasNewState) {
      this.assignNewTransformAndDraw(newState);
    }

    // Reset distance when right joystick is pressed
    if (resetButton.pressed) {
      newState[type].x = 0;
      newState[type].y = 0;
      this.assignNewTransformAndDraw(newState);
    }
  }


  // ---- Helper function ----
  assignNewTransformAndDraw(transform: Transform) {
    this.state.assignNewTransform(transform);
    this.callback();
  }


  // ---- Static helpers ----
  static getEligibleAxesFromKeyMap<Axes extends string>(keyMap: KeyboardModifier<Axes>, e: MouseEvent) {
    const usedAxes: Axes[] = [];
    for (const [ axis, axisKeyList ] of Object.entries(keyMap)) {
      for (const axisKeys of axisKeyList as string[][]) {
        // Check if every present and non present axis modifier matches e exactly
        if (Controls3D.MODIFIER_KEYS.every(modKey => e[modKey] === axisKeys.includes(modKey))) {
          usedAxes.push(axis as Axes);

          if (axisKeys.includes('ctrlKey')) {
            e.preventDefault();
          }
          break;
        }
      }
    }
    return usedAxes;
  }

  /** Compute the distance between two supplied Touch objects. */
  static getTouchesDistance(touch0: Touch, touch1: Touch): number {
    return Math.sqrt(
        Math.pow(touch1.screenX - touch0.screenX, 2)
      + Math.pow(touch1.screenY - touch0.screenY, 2));
  }

  /** Compute the midpoint vector of two supplied Touch objects. */
  static getTouchesMidpoint(touch0: Touch.TouchData, touch1: Touch.TouchData): [number, number] {
    return [
      (touch0.clientX + touch1.clientX) / 2,
      (touch0.clientY + touch1.clientY) / 2,
    ];
  }
}
