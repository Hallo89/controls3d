'use strict';
class State3DAnimationError extends Error {
    constructor(...args) {
        super(...args);
        this.name = 'State3DAnimationError';
    }
}
export class State3D {
    /**
     * Easing functions that can be used within {@link animate}.
     *
     * Mostly taken from https://easings.net/
     */
    static Easing = {
        LINEAR: x => x,
        EASE_IN_SINE: x => 1 - Math.cos((x * Math.PI) / 2),
        EASE_IN_QUAD: x => x * x,
        EASE_IN_CUBIC: x => x * x * x,
        EASE_OUT_SINE: x => Math.sin((x * Math.PI) / 2),
        EASE_OUT_QUAD: x => 1 - (1 - x) * (1 - x),
        EASE_OUT_CUBIC: x => 1 - Math.pow(1 - x, 3),
        EASE_IN_OUT_SINE: x => -(Math.cos(Math.PI * x) - 1) / 2,
        EASE_IN_OUT_QUAD: x => x < 0.5 ? 2 * x * x : 1 - Math.pow(-2 * x + 2, 2) / 2,
        EASE_IN_OUT_CUBIC: x => x < 0.5 ? 4 * x * x * x : 1 - Math.pow(-2 * x + 2, 3) / 2,
        EASE: x => {
            return x < 0.2059
                ? (5.2 * Math.pow(x, 1.8))
                : (1 - 1.3 * Math.pow(1 - x, 2.7));
        },
    };
    // Animation helper properties
    #animationID = {
        scale: -1,
        tran: -1,
        rot: -1
    };
    #animationInit = {
        scale: null,
        tran: null,
        rot: null
    };
    transform = {
        scale: {
            x: 1,
            y: 1,
            z: 1
        },
        tran: {
            x: 0,
            y: 0,
            z: 0
        },
        rot: {
            x: 0,
            y: 0,
            z: 0
        }
    };
    // State aliases
    get scale() {
        return this.transform.scale;
    }
    ;
    set scale(val) {
        this.transform.scale = val;
    }
    ;
    get tran() {
        return this.transform.tran;
    }
    ;
    set tran(val) {
        this.transform.tran = val;
    }
    ;
    get rot() {
        return this.transform.rot;
    }
    ;
    set rot(val) {
        this.transform.rot = val;
    }
    ;
    constructor(initialTransform) {
        if (initialTransform) {
            this.assignNewTransform(initialTransform);
        }
    }
    // ---- Animation ----
    /**
     * Same as {@link animate} but animate from the passed transform
     * to the current transform without additional steps.
     * @param duration How long the animation should run.
     */
    animateFrom(duration, transform, options) {
        return this.animate(duration, {
            ...options,
            steps: { 0: transform }
        });
    }
    /**
     * Same as {@link animate} but animate from the current transform
     * to the passed transform without additional steps.
     * @param duration How long the animation should run.
     */
    animateTo(duration, transform, options) {
        return this.animate(duration, {
            ...options,
            steps: { 100: transform }
        });
    }
    /**
     * Animate this state's {@link transform} based on the passed
     * {@link steps} for the specified amount of milliseconds.
     * @param duration How long the animation should run.
     */
    animate(duration, { steps, callback, easing = State3D.Easing.LINEAR, useStacking = false, fill = true, }) {
        if (!steps)
            throw new State3DAnimationError("The argument `steps` (animation steps) has been omitted but is required.");
        const usedTypes = [];
        for (const transform of Object.values(steps)) {
            for (const type in transform) {
                if (!usedTypes.includes(type)) {
                    usedTypes.push(type);
                }
            }
        }
        const processedSteps = Object.entries(steps)
            .map(val => {
            // @ts-ignore Changing the type from string to number
            val[0] = (Number(val[0]) / 100) * duration;
            return val;
        })
            .sort((val1, val2) => val1[0] > val2[0] ? 1 : -1);
        // TODO fill steps' transformations forward by proportion
        // (e.g. step 1: { x: -5, y: 5 }, step 2: { x: 5 })
        // BUT y should get continue to be animated independently.
        if (!useStacking && usedTypes.every(type => this.#animationID[type] === -1)) {
            for (const type in this.transform) {
                if (this.#animationID[type] === -1) {
                    this.#setInitialAnimationTransform(type);
                }
            }
        }
        let [stepDuration, transform] = processedSteps[0];
        let currentAnimationID = Infinity;
        let prevTransform = this.#animationInit;
        let prevDuration = 0;
        return new Promise(resolve => {
            this.#animationLoop(duration, ({ timeElapsedClamped }) => {
                // ----- START HEADER -----
                // This is always the second frame
                if (currentAnimationID === Infinity) {
                    const targetAnimationID = usedTypes.reduce((acc, curr) => {
                        return Math.max(acc, this.#animationID[curr]);
                    }, -1);
                    currentAnimationID = 1 + targetAnimationID;
                    for (const type of usedTypes) {
                        this.#animationID[type] = currentAnimationID;
                        // If stacking (used with the scroll) is used, continuously advance the initial state
                        if (useStacking) {
                            this.#setInitialAnimationTransform(type);
                        }
                    }
                }
                else if (usedTypes.some(key => currentAnimationID < this.#animationID[key])) {
                    // Abort if another animation has started on one of the currently
                    // animating states and has reached the second frame
                    return true;
                }
                // ----- END HEADER -----
                if (timeElapsedClamped > stepDuration) {
                    prevTransform = State3D.fillTransform(transform);
                    prevDuration = stepDuration;
                    const nextStep = searchNextStep(timeElapsedClamped);
                    if (nextStep) {
                        [stepDuration, transform] = nextStep;
                    }
                    else {
                        stepDuration = duration;
                        transform = this.#animationInit;
                    }
                }
                const stepModifier = easing((timeElapsedClamped - prevDuration) / (stepDuration - prevDuration));
                const isFinished = timeElapsedClamped === duration;
                if (isFinished) {
                    // If fill is used and the current transform is a 100% item
                    const finalTransform = (fill && duration === stepDuration)
                        ? transform
                        : this.#animationInit;
                    this.assignNewTransform(finalTransform);
                }
                else {
                    for (const [type, single] of Object.entries(transform)) {
                        for (const [axis, value] of Object.entries(single)) {
                            const anchorValue = prevTransform[type][axis];
                            this.transform[type][axis] = anchorValue + (stepModifier * (value - anchorValue));
                        }
                    }
                }
                callback?.({
                    animation: {
                        isFinished,
                        elapsed: timeElapsedClamped,
                        duration: duration,
                        types: usedTypes,
                    }
                });
            })
                .then(didCancel => {
                // Only reset animationID to "waiting state" if the animation has
                // not been overridden by another one
                if (!didCancel) {
                    for (const type of usedTypes) {
                        this.#animationID[type] = -1;
                    }
                }
                resolve();
            });
        });
        function searchNextStep(currentDuration) {
            let i = 0;
            while (i < processedSteps.length) {
                const step = processedSteps[i++];
                if (step[0] > currentDuration)
                    return step;
            }
            return null;
        }
    }
    #animationLoop(maxDuration, callback) {
        return new Promise(resolve => {
            let timeStart;
            requestAnimationFrame(step);
            function step(now) {
                // The first frame is empty to start the timings
                if (timeStart == null) {
                    timeStart = now;
                    requestAnimationFrame(step);
                    return;
                }
                const timeElapsed = now - timeStart;
                const timeElapsedClamped = Math.min(timeElapsed, maxDuration);
                const isLastFrame = timeElapsed >= maxDuration;
                const shouldCancel = callback({
                    now,
                    timeStart,
                    timeElapsed,
                    timeElapsedClamped,
                    isLastFrame
                });
                if (!isLastFrame && !shouldCancel) {
                    requestAnimationFrame(step);
                }
                else {
                    resolve(!!shouldCancel);
                }
            }
        });
    }
    #setInitialAnimationTransform(type) {
        this.#animationInit[type] = Object.assign({}, this.transform[type]);
    }
    // ---- Helper functions ----
    /** Deeply clone and return the current transform. */
    cloneTransform() {
        return State3D.cloneTransform(this.transform);
    }
    /** Assign a new transform. */
    assignNewTransform(newTransform) {
        for (const type in newTransform) {
            Object.assign(this.transform[type], newTransform[type]);
        }
        return this;
    }
    // ---- Static helpers ----
    /**
     * Modify a given transform such that missing values are filled with
     * a 0, thus asserting the transform to be Whole instead of Partial.
     *
     * This modifies the given transform in-place.
     * If you need a copy, you must explicitly pass one.
     */
    static fillTransform(transform) {
        for (const type of ['scale', 'tran', 'rot']) {
            transform[type] ??= {};
            for (const axis of ['x', 'y', 'z']) {
                transform[type][axis] ??= 0;
            }
        }
        return transform;
    }
    /**
     * Returns a new transform which is the accumulation
     * of the values of passed transforms.
     * @param transforms The transforms that will be added together.
     */
    static accumulateTransforms(...transforms) {
        const finalTransform = {};
        for (const transform of transforms) {
            for (const [type, transformSingle] of Object.entries(transform)) {
                if (!finalTransform[type]) {
                    finalTransform[type] = {};
                }
                for (const [axis, value] of Object.entries(transformSingle)) {
                    finalTransform[type][axis] = (finalTransform[type][axis] ?? 0) + value;
                }
            }
        }
        return finalTransform;
    }
    static cloneTransform(transform) {
        if (transform instanceof State3D) {
            transform = transform.transform;
        }
        const clone = {};
        for (const stateName in transform) {
            clone[stateName] = Object.assign({}, transform[stateName]);
        }
        return clone;
    }
}
